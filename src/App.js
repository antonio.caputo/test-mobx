import React from "react";
import { Reset as CSSReset } from "styled-reset";
import { RouteGuard, Buttons, Card, Header, LayoutSwitcher, Drawer } from "./components";
import styled from "styled-components";
import { useStores } from "./hooks";
import Sidebar from "./components/Sidebar";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { RootPage } from "./pages/RootPage"
import { ROUTES } from "./routes" 


const MainLayout = styled.div`
  flex: 1;
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  width: 100%;
`;

MainLayout.defaultProps = {
  className: "ws-main-layout",
  dataTestId: "ws-main-layout",
};

/**
 * @name App
 *
 * @description
 */
export const App = () => {
  const { UserStore } = useStores();
  return (
    <MainLayout>
      <CSSReset />
      <Sidebar />
      <Router>
        <Switch>
          <RouteGuard
            path={ROUTES.MAIN}
            component={RootPage}
            redirect={"auth/login"}
          />
        </Switch>
      </Router>
    </MainLayout>
  );
};
