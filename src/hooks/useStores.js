import { useContext } from "react";
import { StoresContext } from "../context";
/**
 * @name useStores
 *
 * @description
 * ...
 */
export const useStores = () => useContext(StoresContext);
