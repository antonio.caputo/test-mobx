import { makeAutoObservable } from "mobx";
import { element } from "prop-types";

/**
 * @name UIStore
 *
 * @description
 * Handle all the UI current state (e.g. sidebar, notification, modal) this store shouldn't affect
 * the domain / data stores
 *
 *
 */
class UIStore {
  sideBarIsOpen = false;
  notificaitonIsOpen = false;
  modalIsOpen = false;
  subHeaderElements = [];
  currentPathName = ''
  layout = {
    rows: false,
    columns: true,
    small: false
  };

  constructor() {
    makeAutoObservable(this);
  }

  // get
  get getCurrentActiveLayout() {
    const active = Object.keys(this.layout).filter((k) => this.layout[k]);
    return active[0];
  }

  get isSidebarOpen() {
    return this.sideBarIsOpen;
  }

  setCurrentActiveLayout = (key) => {
    Object.keys(this.layout).map((k) => (this.layout[k] = false));
    this.layout[key] = true;
  };

  toggleSidebar = (force) => {
    if (force && typeof force === "boolean") this.sideBarIsOpen = force;
    else this.sideBarIsOpen = !this.sideBarIsOpen;
  };

  updateLocation = (pathname) => {
   this.currentPathName = pathname
  };

  get getSubheaderElements() {
    const elements = this.currentPathName.split("/").splice(1,3)
    if (elements.indexOf("subactions") > 1) return "with_actions"
    else return "with_children"
  }

}

export default UIStore;
