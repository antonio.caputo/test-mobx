import { makeAutoObservable } from "mobx";

class UserStore {
  loggedIn = true;
  user = {}
  groups = [];
  constructor() {
    makeAutoObservable(this);
  }

  me = () => {
    return new Promise(resolve => {
      setTimeout( () => resolve({
        birthday: "1979-06-05T00:00:00+00:00",
        connected_profiles: [],
        created: 1610384967,
        email: "antonio.caputo@weschool.com",
        enabled: true,
        id: 14467,
        is_under13: false,
        language: "it_IT",
        last_login: "2021-01-11T17:09:40+00:00",
        locale: "it",
        name: "Antonio",
        should_use_tracking: true,
        status: "Enabled",
        surname: "Caputo",
        tours: [],
        uid: "d2daa91b08a345b14b2b159d7f3e24089cfc31ed2f39d980cec626089e283896",
        username: "antonio.caputo@weschool.com",
        uuid: "6b8556ee-03e3-4a4e-978e-7fff39caa524",
      }
        
      ), 1500)
    })
  }

  get lastGroup() {
    return this.groups[this.groups.length - 1];
  }

  get isLoggedin() {
    return this.loggedIn;
  }

  get isUnderAge() {
    return this.user.is_under13;
  }

  get createRedirectURL() {
    if(this.isUnderAge) {
      return "auth/underage/login"
    }
  }
}

export default UserStore;
