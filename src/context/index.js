import React, { createContext } from "react";
import { toJS } from "mobx";
import { UIStore, UserStore } from "../stores";

const rootStore = {
  UserStore: new UserStore(),
  UIStore: new UIStore()
};

const StoresContext = createContext(rootStore);

const StoreProvider = ({ children }) => (
  <StoresContext.Provider value={{ ...rootStore }}>
    {children}
  </StoresContext.Provider>
);

/**
 * DEBUG IN CHROME CONSOLE ONLY IN DEV MODE
 * @example
 * window.stores.UserStore.method() or property
 * window.MobxDebug(window.stores.UIStore.sidebarIsOpen)
 */
// if( process.env.NODE_ENV === 'development') { ..
window.rootStore = rootStore;
window.MobxDebug = (value) => toJS(value);

export { StoreProvider, StoresContext };
