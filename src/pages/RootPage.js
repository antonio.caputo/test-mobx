import React from "react";
import { Grid, Row, Col } from "react-flexbox-grid";
import {
  Buttons,
  Card,
  Header,
  LayoutSwitcher,
  SubHeader,
  Content,
} from "../components";
import { ROUTES } from "../routes";
import { Route, Link, NavLink } from "react-router-dom";

export const RootPage = () => {
  return (
    <>
      <Header hasNavTrigger navItems={<Buttons />}>
        <SubHeader className="ws-subheader">
          <ul>
            <li>
              <NavLink
                activeClassName="is-selected"
                to={"/group/45657879877/wall"}
              >
                Wall
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName="is-selected" to={ROUTES.BOARDS}>
                Boards
              </NavLink>
            </li>
            <li>
              <a href="">Test</a>
            </li>
            <li>
              <a href="">Live</a>
            </li>
            <li>
              <Link to={"/group/45657879877/subactions"}>Subactions</Link>
            </li>
          </ul>
        </SubHeader>
      </Header>

      <Content className="ws-content">
        <LayoutSwitcher>
          <Grid fluid>
            <Route
              exact
              path={ROUTES.GROUPS}
              component={() => <div> groups </div>}
            />
            <Route
              exact
              path={ROUTES.WALL}
              component={() => <div> wall </div>}
            />
            <Route
              exact
              path={ROUTES.BOARDS}
              component={() => <div> boards </div>}
            />

            <span>fluid grid</span>
            <Row>
              <Col xs={12} md={4}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={4}>
                <Card title={"Card 2 "} />
              </Col>
              <Col xs={12} md={4}>
                <Card title={"Card 3 "} />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={6}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={6}>
                <Card title={"Card 2 "} />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={1}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={11}>
                <Card title={"Card 2 "} />
              </Col>
            </Row>
          </Grid>
          <Grid>
            <span>fixed grid</span>
            <Row>
              <Col xs={12} md={4}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={4}>
                <Card title={"Card 2 "} />
              </Col>
              <Col xs={12} md={4}>
                <Card title={"Card 3 "} />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={6}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={6}>
                <Card title={"Card 2 "} />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={1}>
                <Card title={"Card 1 "} />
              </Col>
              <Col xs={12} md={11}>
                <Card title={"Card 2 "} />
              </Col>
            </Row>
          </Grid>
        </LayoutSwitcher>
      </Content>
    </>
  );
};
