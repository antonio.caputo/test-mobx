export const BASE = "/";

export const ROUTES = {
  MAIN: `${BASE}`,
  WALL: `${BASE}group/:id/wall`,
  BOARDS: `${BASE}boards`,
  GROUPS: `${BASE}groups`,
  GROUP: `${BASE}group/:id`,
};
