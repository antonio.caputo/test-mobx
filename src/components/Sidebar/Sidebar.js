import React from "react";
import Drawer from "../Drawer";
import { observer } from "mobx-react";
import { useStores } from "../../hooks";

const Sidebar = ({ open, toggleCallback }) => {
  const { UIStore } = useStores()
  const { isSidebarOpen, toggleSidebar } = UIStore
  
  return (
    <Drawer
      open={isSidebarOpen}
      onDismiss={() => toggleSidebar()}
    >
      <button onClick={() => toggleSidebar() }>close</button>
    </Drawer>
  );
};

export default observer(Sidebar);
