import { element, oneOfType, string, node } from "prop-types";
import React from "react";
import { useStores } from "../../hooks";
import { StyledCard } from "./Card.styles";
import { observer } from "mobx-react";

/**
 * @name Card
 *
 * @description
 * ...
 */
const Card = ({ className, dataTestId, children, title }) => {
  const { UIStore } = useStores();
  return (
    <StyledCard data-test-id={dataTestId} className={className}>
      <h2>{title || "Card Title"}</h2>
      <h4>current layout : {UIStore.getCurrentActiveLayout} </h4>
      {children}
    </StyledCard>
  );
};

Card.defaultProps = {
  dataTestId: "ws-card",
  className: "ws-card"
};

Card.propTypes = {
  
  /** @name title {string}  */
  dataTestId: string,

  /** @name title {string}  */
  title: string,

  /** @name children {any}  */
  children: oneOfType([node, element])
};

export default Card;
