import styled from "styled-components";
export const StyledCard = styled.div`
  border-radius: 15px;
  flex: 1 0%;
  align-items: center;
  justify-content: center;
  margin: 15px 0px 15px;
  padding: 10px;
  background: #fff;
  border: 2px solid #37425b;
  border-radius: 6px;
  min-height:100px;
`;
