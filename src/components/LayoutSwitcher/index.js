import React from "react";
import { observer } from "mobx-react";
import { node } from "prop-types";

import { useStores } from "../../hooks";
import { StyledContainer } from "./LayoutSwitcher.styles";

/**
 * @name
 *
 * @description
*/
const LayoutSwitcher = ({ children }) => {
  const { UIStore } = useStores();
  return (
    <StyledContainer layout={UIStore.getCurrentActiveLayout}>
      {/* <h1>{UIStore.getCurrentActiveLayout}</h1> */}
      {children}
      {/** or any another conditional rendering based on getCurrentActiveLayout... */}
    </StyledContainer>
  );
};

LayoutSwitcher.propTypes = {
  children: node
};

export default observer(LayoutSwitcher);
