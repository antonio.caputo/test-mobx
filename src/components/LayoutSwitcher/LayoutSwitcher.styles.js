import styled, { css } from "styled-components";

export const StyledContainer = styled.div`
  margin: 15px auto;
  h1 {
    font-size:48px;
  }

  ${({ layout }) =>
    layout === "rows" &&
    css`
      flex-direction: column;
      border-color: blue;
    `}

  ${({ layout }) =>
    layout === "columns" &&
    css`
      border-color: red;
    `}

  ${({ layout }) => layout === "small" && css`
    width: 768px;
    border-color: green;
    /* .row {
      display: flex;
      flex-direction: column;
    }
    div[class*='col'] {
      width:100% !important;
    } */
  `}
`;
