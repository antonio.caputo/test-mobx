import styled from "styled-components"
export const StyledContent = styled.main`
  flex: 1;
  padding-top: 120px;
  padding-bottom: 35px;
`;
