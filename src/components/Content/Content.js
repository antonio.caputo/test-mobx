import React from "react";
import { StyledContent } from "./Content.styles";
const Content = ({ children }) => {
  return <StyledContent>{children}</StyledContent>;
};

export default Content;
