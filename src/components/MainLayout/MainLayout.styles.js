export const MainLayout = styled.div`
    flex: 1;
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    width: 100%;
`;