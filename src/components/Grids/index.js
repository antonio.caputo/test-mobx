import React from "react"
import { Grid as G, Row, Col } from "react-flexbox-grid"
import styled from "styled-components"
import { any } from "prop-types"

const StyledGrid = styled(G)`
  padding-right: 15px;
  padding-left: 15px;

  /* 
  @media screen and (min-width: 1280px) {
    width: 1184px;
    max-width: 1184px;
  }

  @media screen and (min-width: 1920px) {
    width: 1740px;
    max-width: 1740px;
  } 
  */
`

const Grid = ({ children, ...props }) => {
  return <StyledGrid {...props}>{children}</StyledGrid>
}

Grid.propTypes = {
  children: any,
}

export { Grid, Row, Col }
