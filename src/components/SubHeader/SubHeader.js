import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { SubHeaderStyles } from "./SubHeader.styles";
import { useStores } from "../../hooks";
import { Row, Col, Grid } from "../Grids";

const SUBHEADER_TYPE = {
  WITH_ACTIONS: "with_actions",
  WITH_CHILDREN: "with_children",
};

const SubHeader = ({ children, ...props }) => {
  const { UIStore } = useStores();

  /*  const PickSubHeader = () => {
    if (mode === SUBHEADER_TYPE.WITH_CHILDREN) {
      return children
    } else {
      return <div>Sub header with actions</div>
    }
  }; */

  UIStore.updateLocation(props.location.pathname);
  const mode = UIStore.getSubheaderElements
  return (
    <SubHeaderStyles>
      <Grid fluid>
        <Row>
          <Col xs={12}>
            { mode === SUBHEADER_TYPE.WITH_CHILDREN &&
              children}
            { mode === SUBHEADER_TYPE.WITH_ACTIONS && (
              <div>
                <span onClick={() => window.history.back(-1)} style={{fontSize:25, cursor:'pointer'}}>&lt; </span> 
                <span>Sub header with actions</span>
              </div>
            )}
          </Col>
        </Row>
      </Grid>
    </SubHeaderStyles>
  );
};

export default withRouter(SubHeader);
