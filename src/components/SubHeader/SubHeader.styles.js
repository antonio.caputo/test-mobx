import styled from "styled-components"

export const SubHeaderStyles = styled.div`
  background-color: #fff;
  color: #181818;
  box-shadow: 3px 1px 3px #e9edf9;
  height: 60px;
  display: flex;
  align-items: center;

  ul {
    display: flex;
    li {
      align-items: center;
      display: flex;
      justify-content: center;
      min-width: 140px;
      a {
        text-transform: uppercase;
        color: #000;
        border-bottom: 4px solid transparent;
        cursor: pointer;
        text-decoration: none;
        line-height: 56px;
        width: 100%;
        text-align: center;
        &.is-selected {
          border-color: #37425b;
        }
      }
    }
  }

`;