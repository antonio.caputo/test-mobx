import { observer } from "mobx-react";
import React from "react";
import { useStores } from "../../hooks";

const CurrentLayout = () => {
  const { UIStore } = useStores();
  return <b>{UIStore.getCurrentActiveLayout}</b>;
};

export default observer(CurrentLayout);
