import React from "react";
import { useStores } from "../../hooks";
import CurrentLayout from "../CurrentLayout";
import styled from "styled-components";

const StyledButtons = styled.div`
 
`;
/**
 *
 * @name Buttons
 *
 * @description
 */
const Buttons = () => {
  const { UIStore } = useStores();
  return (
    <StyledButtons>
      <ul>
        <li onClick={() => UIStore.setCurrentActiveLayout("columns")}>
          Columns
        </li>
        <li onClick={() => UIStore.setCurrentActiveLayout("rows")}>
          Rows
        </li>
        <li onClick={() => UIStore.setCurrentActiveLayout("small")}>
          Small
        </li>
      </ul>
    </StyledButtons>
  );
};

export default Buttons;
