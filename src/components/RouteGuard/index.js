import React, { useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import { elementType, string, bool, any } from "prop-types";

class FakeUserService {
  checkAccess = async () => {
    try {
      return new Promise((resolve) => {
        setTimeout(() => resolve(true));
      }, 2500);
    } catch (e) {
      console.log("UserService.checkAccess :: error", e);
    }
  };
}

/**
 * @name RouteGuard
 *
 * @description
 * Protect a component from access. It's used at Route level.
 *
*/
const RouteGuard = ({
  dataTestId,
  component: Component,
  redirect,
  showLoader,
  ...rest
}) => {
  const [authCheck, setAuth] = useState(false);
  const [tokenValidationFinished, setTokenValidationFinished] = useState(false);

  const verifyUser = async () => {
    const service = new FakeUserService();
    const logged = await service.checkAccess();
    if (logged) {
      setAuth(true);
      setTokenValidationFinished(true);
    } else {
      setAuth(false);
      setTokenValidationFinished(true);
    }
  };

  useEffect(() => {
    verifyUser();
  }, []);

  if (!tokenValidationFinished)
    return showLoader ? <span>loading...</span> : null;

  return (
    <Route
      data-test-id={dataTestId}
      {...rest}
      render={(props) =>
        authCheck ? (
          <Component {...props} />
        ) : (
          /*  Redirect component doesn't work, the user should be force */
          <Redirect
            push
            to={{
              pathname: redirect,
              search: "?from=anyparam",
            }}
          />
        )
      }
    />
  );
};

RouteGuard.displayName = "RouteGuard";

RouteGuard.defaultProps = {
  dataTestId: "ws-route-guard",
};

RouteGuard.propTypes = {
  /** @prop {React|Component} component The component to show if the access validation return true */
  component: any,

  /** @prop {string} redirect url for redirect */
  redirect: elementType.isRequired,

  /** @prop {boolean} showLoader Show or not a loader */
  showLoader: bool,

  /** @prop {string} dataTestId Show or not a loader */
  dataTestId: string,
};

export default RouteGuard;
