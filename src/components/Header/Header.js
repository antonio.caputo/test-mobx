import React from "react";
import { StyledHeader, Toggle, Logo } from "./Header.styles";

import { Grid, Row, Col } from "../Grids";
import { useToggle, useStores } from "../../hooks";

const Header = ({
  dataTestId,
  className,
  hasNavTrigger,
  hasLogo,
  navItems,
  children,
  ...props
}) => {
 
  const { UIStore } = useStores()

  // using store and toggle hook
  // const [on, toggle] = useToggle(true);

  return (
    <StyledHeader className={className} data-test-id={dataTestId} {...props}>
      <Grid fluid>
        <Row>
          <nav>
            {hasNavTrigger && (
              <Toggle
                onClick={() => {
                  UIStore.toggleSidebar();
                  // toggle(!on)
                }} 
              >
                <i>&#43;</i>
              </Toggle>
            )}

            {hasLogo && <Logo>Un nuovo gruppo in cui invitare</Logo>}

            {/* {React.cloneElement(navItems, {
              className: `${on ? `active` : ``}`,
            })} */}
             {React.cloneElement(navItems)}
          </nav>
        </Row>
      </Grid>
      {children}
    </StyledHeader>
  );
};

Header.defaultProps = {
  hasLogo: true,
  hasNavTrigger: true,
  className: "ws-navbar",
  dataTestId: "ws-navbar",
};

export default Header;
