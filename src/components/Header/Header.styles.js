import styled from "styled-components";

export const Toggle = styled.div`
  font-size:35px;
  width: 45px;
  text-align:center;
  cursor:pointer;
`;


export const Logo = styled.div`
  width:100%;
  font-size: 1.8rem;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const StyledHeader = styled.header`
  top: 0;
  left: 0;
  z-index: 900;
  width: 100%;
  position: fixed;
  background: #3d7dea;
  height: 60px;
  color:#fff;
  nav { 
    display:flex;
    align-items:center;
    height: 60px;
    width: 100%;
  }
  
  .active {
    display: flex;
  }

  @media screen and (min-width: 800px) {
    nav ul {
      display: flex;
      justify-content: flex-end;
      flex-direction: row;
      li {
        display: inline-block;
        margin: 0 15px 0 0;
        text-align: left;
        border-bottom-style: none;
        padding: 0;
      }
    } 

  }
`;
