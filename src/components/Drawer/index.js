import React from "react"
import { oneOfType, bool, func, oneOf, string, number, element } from "prop-types"
import { DrawerWrapper, DrawerOverlay, DrawerContent } from "./Drawer.styles"

/**
 * @name Drawer
 *
 * @description
 */
const Drawer = ({ children, ...props }) => {
  const { open, size, position, onDismiss, backgroundColor } = props
  return (
    <>
      <DrawerWrapper open={open} size={size} position={position}>
        <DrawerOverlay open={open} onClick={onDismiss} />
        <DrawerContent
          open={open}
          size={size}
          position={position}
          backgroundColor={backgroundColor}
        >
          {children}
        </DrawerContent>
      </DrawerWrapper>
    </>
  )
}

Drawer.propTypes = {
  open: bool,
  size: oneOfType([string, number]),
  position: oneOf(["top", "bottom", "left", "right"]),
  onDismiss: func.isRequired,
  backgroundColor: string,
  children: element,
}

Drawer.defaultProps = {
  size: "300px",
  position: "left",
  backgroundColor: "#3d7dea",
}

export default Drawer
