import styled, { css } from "styled-components"

const transforms = {
  top: "translateY(-100%)",
  right: "translateX(100%)",
  bottom: "translateY(100%)",
  left: "translateX(-100%)",
}
const placements = {
  top: {
    top: 0,
    right: 0,
    left: 0,
  },
  right: {
    top: 0,
    right: 0,
    bottom: 0,
  },
  bottom: {
    right: 0,
    bottom: 0,
    left: 0,
  },
  left: {
    top: 0,
    bottom: 0,
    left: 0,
  },
}


export const DrawerWrapper = styled.div`
 z-index: 999;
  display: block;
  width: ${(props) =>
    props.position !== "top" && props.position !== "bottom" && props.size
      ? props.size
      : "300px"};
  height: ${(props) =>
    (props.position === "top" || props.position === "bottom") && props.size
      ? props.size
      : "100%"};
  transform: ${(props) => (!props.open ? transforms[props.position] : null)};
`

export const DrawerOverlay = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  z-index: 998;
  display: ${(props) => (props.open ? null : "none")};
  background-color: rgb(0 0 0 / 48%);
`

export const DrawerContent = styled.div`
  display: block;
  position: fixed;
  ${(props) => placements[props.position]}
  z-index: 999;
  width: ${(props) =>
    props.position !== "top" && props.position !== "bottom" && props.size
      ? props.size
      : "300px"};
  transform: ${(props) => (!props.open ? transforms[props.position] : null)};
  transition: transform 0.2s ease-out;
  overflow-x: hidden;
  color: #000;
  background-color: ${(props) => props.backgroundColor || "#fff"};
  box-shadow: -10px 0px 10px rgba(0, 0, 0, 0.19);

  ${(props) => props.open && css`
      padding:15px;
  `}

  @media screen and (max-width:476px) {
    width:100%;
  }
  
`